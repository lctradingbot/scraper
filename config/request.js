module.exports = {
    options: {
        method: 'POST',
        url: 'https://www.nordnet.se/api/2/batch',
        headers:
        {
            'cache-control': 'no-cache',
            'client-id': 'NEXT',
            'Sec-Fetch-Site': 'same-origin',
            Referer: 'https://www.nordnet.se/',
            accept: 'application/json',
            'content-type': 'application/json',
            ntag: '{{ntag}}',
            Connection: 'keep-alive',
            Cookie: 'nnanon=797d9ac0-f559-11e9-bb07-f599a9f53fb3; _ga=GA1.2.1508534237.1571810026; __qca=P0-1639289662-1571810026514; _gid=GA1.2.117747447.1572791643; _gat_UA-58430789-7=1; NEXT={{NEXT}}; coid={{coid}}',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
            'Accept-Language': 'en-US,en;q=0.9',
            'x-nn-href': 'https://www.nordnet.se/marknaden/aktiekurser/16105612-nel',
            'Accept-Encoding': 'gzip, deflate, br',
            Origin: 'https://www.nordnet.se',
            'Sec-Fetch-Mode': 'cors'
        },
        pipeline: [
            {
                body: {
                    batch:
                        `[

                            { \"relative_url\": \"watchlists/instrument/{{id}}\", \"method\": \"GET\" },
                            { \"relative_url\": \"instruments/historical/returns/{{id}}?local_currency=true\", \"method\": \"GET\" },
                            { \"relative_url\": \"instruments/historical/prices/{{id}}?fields=open,high,low,last,volume&from=2019-10-08\", \"method\": \"GET\" },
                            { \"relative_url\": \"instruments/price/{{id}}?request_realtime=false\", \"method\": \"GET\" },
                            { \"relative_url\": \"instruments/{{id}}/trades?count=4\", \"method\": \"GET\" },
                            { \"relative_url\": \"instruments/statistics/{{id}}\", \"method\": \"GET\" },
                            { \"relative_url\": \"company_information/summary/{{id}}\", \"method\": \"GET\" },
                            { \"relative_url\": \"news_sources\", \"method\": \"GET\" },
                            { \"relative_url\": \"user/settings/newsSourceFilter\", \"method\": \"GET\" },
                            { \"relative_url\": \"company_information/event/{{id}}\", \"method\": \"GET\" },
                        ]`
                }
            },
            {
                body: {
                    batch: `[


                        { \"relative_url\": \"shareville/comments/instrument/{{id}}?limit=5\", \"method\": \"GET\" },
                        { \"relative_url\": \"company_information/financial/{{id}}\", \"method\": \"GET\" },
                        { \"relative_url\": \"instruments/validation/suitability/{{id}}\", \"method\": \"GET\" },
                        { \"relative_url\": \"instruments/trading_status/{{id}}\", \"method\": \"GET\" },
                        { \"relative_url\": \"instruments/historical/returns/{{id}}?show_details=true\", \"method\": \"GET\" },
                        { \"relative_url\": \"user/settings/graphs\", \"method\": \"GET\" },
                        { \"relative_url\": \"suitability/assessment/0\", \"method\": \"GET\" },
                        { \"relative_url\": \"instruments/{{id}}?positions=1,2\", \"method\": \"GET\" },

                    ]`
                },
            },
            {
                body: {
                    batch: `[

                        { \"relative_url\": \"news?instrument_id={{id}}&source_id=&news_lang=sv,en&limit=8&offset=0\", \"method\": \"GET\" },
                        { \"relative_url\": \"news?source_id=&news_lang=sv,en&limit=8&offset=0\", \"method\": \"GET\" },
                    ]`
                }
            },
            {
                body: {
                    batch: `[
                        { \"relative_url\": \"instruments/16287318,16088882,16984711,17075870,16945976\", \"method\": \"GET\" },
                        { \"relative_url\": \"instruments/price/{{id}}\", \"method\": \"GET\" },
                    ]`
                }
            },
            {
                body: {
                    batch: `[
                        { \"relative_url\": \"https://www.nordnet.se/api/2/instruments/price/16287318%2C16088882%2C16984711%2C17075870%2C16945976\", \"method\": \"GET\" },
                    ]`
                }
            },
            {
                name: 'order-depth',
                body: {
                    batch: `[
                        { \"relative_url\": \"instruments/price/{{id}}?request_realtime=false\", \"method\": \"GET\", \"path\": \"/order-depth\" },
                        { \"relative_url\": \"instruments/depth/{{id}}\", \"method\": \"GET\" }
                    ]`
                }
            }
        ]
    }
}