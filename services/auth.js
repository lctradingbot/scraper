const puppeteer = require('puppeteer'),
    authConf = require('../config/auth'),
    url = 'https://www.nordnet.se/login?redirect_to=%2Fse%3Fsv.url%3D12.66cf2f761697cd534c51f3',
    cookieKeys = authConf.cookies.map(item => item.from),
    tokens = [];

module.exports = {
    tokens: undefined,
    setTokens: async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        const account = {
            user: process.env._user,
            pass: process.env._pass
        };


        const selector = 'button.button.link';
        await page.goto(url);
        await page.waitForSelector(selector);

        if (!account.user || !account.pass)
            throw new Error('User and pass needs to be set at start script (look for _user and _pass at package.json)!');
        await page.evaluate((account) => {
            document.querySelector('button.button.link').click();
            document.getElementById('username').value = account.user;
            document.getElementById('password').value = account.pass;
            document.querySelector('button.button[type=submit]').click();
        }, account);
        await page.waitForNavigation();
        await addTokens(await page.cookies());
        let html = await page.evaluate(() => document.body.innerHTML.toString());
        let ntag = html.match(/(?<=\\"ntag\\":\\")(.*?)(?=\\"\,)/)[0];
        await page.screenshot({ path: 'page-logged-in.png' });
        await browser.close();
        module.exports.tokens = await mapTokens(tokens);
        if (ntag)
            module.exports.tokens.push({ 'ntag': ntag });

        return module.exports.tokens && module.exports.tokens.length === 3 ? true : false;
    }
}

const mapTokens = async (tokensFrom) => {
    let tokensTo = [];
    for (let index = 0; index < tokensFrom.length; index++) {
        const tokenFrom = tokensFrom[index];
        for (let key in tokenFrom) {
            console.log(key)
            let cookieFrom = authConf.cookies.find(item => item.from == key),
                cookieTo = {};
            if (cookieFrom)
                cookieTo[cookieFrom.to] = tokenFrom[key];
            tokensTo.push(cookieTo);
        }

    }
    return tokensTo;
}

const addTokens = async (cookies) => {
    for (let i = 0; i < cookieKeys.length; i++) {
        let find = cookies.find(item => item.name == cookieKeys[i]);
        let prevToken = tokens.find(item => item[cookieKeys[i]]);
        if (find && !prevToken) {
            let token = {};
            token[cookieKeys[i]] = find.value;
            tokens.push(token);
        }
    }
}