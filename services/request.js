const axios = require('axios');
const requestConf = require('../config/request');
const fs = require('fs');
module.exports = {
    getStockData: async (id, tokens) => {
        let options = requestConf.options;
        options.headers = await replaceHeaderTokens(tokens, options.headers);
        for (let index = 0; index < requestConf.options.pipeline.length; index++) {
            const reqBody = requestConf.options.pipeline[index];
            let _options = Object.assign({}, options),
                fileName = reqBody.name ? reqBody.name : index.toString();
            _options.data = await replaceBodyStockId(id, reqBody.body);
            delete _options.pipeline;
            let res = await asyncRequest(_options);
            if (res && res.data) {
                var dir = `./data/${id}`;
                if (!fs.existsSync(dir)) {
                    fs.mkdirSync(dir);
                }
                fs.writeFileSync(`${dir}/${fileName}.json`, JSON.stringify(res.data));
            }

            else
                console.log('Error requesting api!',res);
        }
    }
}

const asyncRequest = async (reqObj) => {
    try {
        reqObj.maxRedirects = 0;
        const response = await axios(reqObj);
        return response;
    }
    catch (error) {
        console.log(error);
        return undefined;
    }
}

const replaceBodyStockId = async (id, body) => {
    return await JSON.parse(JSON.stringify(body).replace(/{{id}}/g, id));
}

const replaceHeaderTokens = async (tokens, headers) => {
    let _headers = JSON.stringify(headers);
    for (let index = 0; index < tokens.length; index++) {
        const token = tokens[index],
            key = Object.keys(token)[0];
        _headers = _headers.replace(new RegExp(`{{${key}}}`, 'g'), token[key]);
    }
    return JSON.parse(_headers);
}