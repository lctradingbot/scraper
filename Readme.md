# Setup

## Prerequesites
Install Node.js from https://nodejs.org/en/download/
Install NPM from https://www.npmjs.com/get-npm

## Installation
$ npm i

## Account configuration
Open package.json find [USER] and [PASS] and replace by your account user and pass respectively (Nordnet acount)

## Run scraper
$ npm run start