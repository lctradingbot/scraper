const authSvc = require('./services/auth'),
    requestSvc = require('./services/request'),
    sharesConf = require('./config/shares'),
    authConf = require('./config/auth');

const runStock = async(id) => {
    if (await authSvc.setTokens()) {
        console.log(authSvc.tokens);
        await requestSvc.getStockData(id, authSvc.tokens);
    };
}


for (let index = 0; index < sharesConf.shares.length; index++) {
    const share = sharesConf.shares[index];
    runStock(share.id);
    
}
